<?php

// 1. Crea un array llamado nombres que contenga varios nombres

echo '<h3>1.Crea un array llamado nombres que contenga varios nombres</h3>';

$nombres = array('Manuel','Gabriel','Iriome','Iago','Jose','Camilo','Dani','Genova','Javi');

print_r($nombres);

// 2. Muestra el número de elementos que tiene el array (función count)

echo '<h3>2.Muestra el número de elementos que tiene el array (función count)</h3>';
echo count($nombres);

// 3. Crea una cadena que contenga los nombres de los alumnos existentes en el array separados por un espacio y muéstrala (función de strings implode)

echo '<h3>3. Crea una cadena que contenga los nombres de los alumnos existentes en el array separados por un espacio y muéstrala (función de strings implode)</h3>';

$nombresSeparados = implode(" ", $nombres);
echo $nombresSeparados;

// 4. Muestra el array ordenado alfabéticamente (función asort).

echo '<h3>4. Muestra el array ordenado alfabéticamente (función asort).</h3>';

asort($nombres);
print_r($nombres);


// 5. Muestra por pantalla las diferencias con ksort y con sort.

echo '<h3>5. Muestra por pantalla las diferencias con ksort y con sort.</h3>';

echo '<p>Usando ksort() (Ordena el array por su clave)</p>';

ksort($nombres);
print_r($nombres);

echo '<p>Usando sort() (Ordena el array por el orden numérico o alfabético de su valor)</p>';

sort($nombres);
print_r($nombres);


// 6. Muestra el array en el orden inverso al que se creó (función array_reverse)

echo '<h3>6. Muestra el array en el orden inverso al que se creó (función array_reverse)</h3>';

$nombres = array('Manuel','Gabriel','Iriome','Iago','Jose','Camilo','Dani','Genova','Javi');
$nombresReversed = array_reverse($nombres);
print_r($nombresReversed);

// 7. Muestra la posición que tiene tu nombre en el array (función array_search)

echo '<h3>7. Muestra la posición que tiene tu nombre en el array (función array_search)</h3>';

$manuel = array_search('Manuel', $nombres);

print_r($manuel);

// 8. Crea un array de alumnos donde cada elemento sea otro array que contenga el id, nombre y edad del alumno.

echo '<h3>8. Crea un array de alumnos donde cada elemento sea otro array que contenga el id, nombre y edad del alumno.</h3>';

$arrayAlumnos = array(
    array(1, 'Manuel', '40'),
    array(2, 'Gabriel', '20'),
    array(3, 'Iriome', '21'),
    array(4, 'Iago', '19'),
    array(5, 'Jose', '20'),
    array(6, 'Camilo', '55'),
    array(7, 'Dani', '30'),
    array(8, 'Genova', '21'),
    array(9, 'Javi', '23'),
);

print_r($arrayAlumnos);

?>
<!--9. Crea una tabla html en la que se muestren todos los datos de los alumnos.  -->

<h3>9. Crea una tabla html en la que se muestren todos los datos de los alumnos.</h3>

<table style="width:50%; border:1px solid black;">
  <tr>
    <th style="border:1px solid black;">Id</th>
    <th style="border:1px solid black;">Nombre</th>
    <th style="border:1px solid black;">Edad</th>
  </tr>
  <?php 
  foreach ($arrayAlumnos as $arrAlum) { ?>
    <tr>
        <td style="border:1px solid black;"><?php echo $arrAlum[0];?></td>
        <td style="border:1px solid black;"><?php echo $arrAlum[1];?></td>
        <td style="border:1px solid black;"><?php echo $arrAlum[2];?></td>
    </tr>
    
    <?php 
  }
  ?>
</table>

<?php 

// 10. Utiliza la función array_column para obtener un array indexado que contenga únicamente los nombres de los alumnos y muéstralo por pantalla.

echo '<h3>10.Utiliza la función array_column para obtener un array indexado que contenga únicamente los nombres de los alumnos y muéstralo por pantalla.</h3>';

$soloNombres = array_column($arrayAlumnos, 1);
print_r($soloNombres);

// 11. Crea un array con 10 números y utiliza la función array_sum para obtener la suma de los 10 números.

echo '<h3>11.Crea un array con 10 números y utiliza la función array_sum para obtener la suma de los 10 números. </h3>';

$numeros = array(1,39,102,4,17,34);
echo '<h4>Números del array:</h4>';
print_r($numeros);

$total = array_sum($numeros);
echo '<h4>Total de la suma:</h4>';
print_r($total);




?>

