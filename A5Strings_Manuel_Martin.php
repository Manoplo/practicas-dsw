

<?php

// MANUEL LUIS MARTÍN PÉREZ -- A5 Strings

// Ejercicio 1.1

$nombrePila = '';

echo '<h2>Ejercicio 1.1</h2>';

if(isset($_GET["nombrePila"])) {
echo "Hola ".$_GET["nombrePila"]. "! <br/>";
}else{
    echo 'Probando DOLARGET <br/>';
}

// Ejercicio 1.2

$usuario;

echo '<h2>Ejercicio 1.2</h2>';

$nombreUsuario = $_GET["usuario"] ?? 'nadie';

echo $nombreUsuario.'<br/>';

// Ejercicio 1.3

echo '<h2>Ejercicio 1.3</h2>';

$nombreUsuarioConEspacios = " Manuel Martín Pérez ";

$nombreUsuarioSinEspacios = trim($nombreUsuarioConEspacios);

var_dump($nombreUsuarioConEspacios,$nombreUsuarioSinEspacios); 

echo $nombreUsuarioSinEspacios;

// Ejercicio 2

echo '<h2>Ejercicio 2</h2>';

$nombre = 'Manuel Martín Pérez';

echo strlen($nombre).'<br/>';

// Ejercicio 3

echo '<h2>Ejercicio 3</h2>';

echo strtoupper($nombre).'<br/>';
echo strtolower($nombre).'<br/>';

// Ejercicio 4

echo '<h2>Ejercicio 4</h2>';

$prefijo = '';

$nombrePilaGet = $_GET['nombrePila'];
$prefijoGet = $_GET['prefijo'];

$pos = strpos($nombrePilaGet,$prefijoGet);

if($pos === 0 ){
    echo "El parámetro'$prefijoGet' se encuentra en '$nombrePilaGet'";
}else{
    echo "El parámetro'$prefijoGet' NO se encuentra en '$nombrePilaGet'";
}



// Ejercicio 5

echo '<h2>Ejercicio 5</h2>';

echo substr_count(strtolower($nombre),'a').'<br/>';

// Ejercicio 6 // Cambio "a" por "0" porque no hay "o". 

echo '<h2>Ejercicio 6</h2>';

echo str_ireplace('a','0',strtolower($nombre)).'<br/>';

// Ejercicio 7 //

echo '<h2>Ejercicio 7</h2>';

$url = 'http://username:password@hostname:9090/path?arg=value';

var_dump(parse_url($url, PHP_URL_SCHEME));
var_dump(parse_url($url, PHP_URL_USER));
var_dump(parse_url($url, PHP_URL_PATH));
var_dump(parse_url($url, PHP_URL_QUERY));



?>