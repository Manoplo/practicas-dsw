<?php declare( strict_types = 1 );

// Ejercicio 1
// declare(strict_types= 1) obliga a las funciones a aceptar tipos exactamente del tipo que se declaran o de lo contrario arrojan un TypeError.

function saludar(string $nombre, int $edad ){
    echo "Soy $nombre y tengo $edad años\n"."<br/><hr/>";
}

saludar('Manuel', 40);

// En php podemos indicar el valor que devuelve una función mediante la siguiente sintáxis:
// function sum($a, $b): float {
//    return $a + $b;
//}

function popularArray(string $value, int $iterations) : array {

    $populatedArray = array();

    for ($i=0; $i <=$iterations ; $i++) { 
        $populatedArray[$i] = $value.$i;
    }

    return $populatedArray;
}

var_dump(popularArray('Valor de Prueba', 10));

// Podemos comprobar si la función está seteada mediante y añadir un signo de interrogación a la indicación del valor devuelto por la función

function get_item(): ?string {
    if (isset($_GET['item'])) {
        return $_GET['item'];
    } else {
        return null;
    }
}

echo get_item(); // Aquí no pintará nada en pantalla dado que la variable no está definida y por tanto devuelve null y la pintará si pasamos un querystring 

?>
<hr>
<?php

// Ejercicio 2

function insert($nombre_tabla, $datos) : string {

    $keysFromArray = implode(",",array_keys($datos));
    $valuesFromArray = implode(", :",array_keys($datos));

    $stm = "insert into $nombre_tabla($keysFromArray) values(:$valuesFromArray)<br/>";
    return $stm;

}

echo insert('alumnos',['id'=>1,'nombre'=>'Manu', 'curso' =>'DAW']);

?>

<?php 

// Ejercicio 3

$cadena = "insert into tabla (campos) values (valores)";

function newInsert($nombre_tabla, $datos, &$cadena){

    $arrayKeys = implode("," , array_keys($datos));
    $arrayValues = implode(", :" , array_keys($datos));
    $cadena = "insert into $nombre_tabla ($arrayKeys) values (:$arrayValues)";

}

newInsert('alumnos', ['id'=>1, 'nombre'=>'Jesús', 'curso'=>'2º DAW'], $cadena);

echo $cadena;

?>

