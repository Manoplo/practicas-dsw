<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manuel Martín A14</title>
</head>
<body>

  
    <h1>¡Bienvenido a Adivina el número 2.0! </h1>

    
    
    <!--CODIGO PHP // CONTROLADOR AQUÍ-->
    
    
    <?php

    $numero = 1723;

    $guessNumber = htmlspecialchars(isset($_POST["guess"])) ? $_POST["guess"] : null;


    // Persistencia de datos. 

    $oldGuess = isset($_POST["guess"]) ? $_POST["guess"] : '';

    


    if($guessNumber == null){

        echo '<h2>No has introducido ningún parámetro</h2>';

    } elseif ( !is_numeric($guessNumber) ){

        echo '<h2>El parámetro no es un número!</h2>';

    } elseif ( $guessNumber < $numero ) {

        echo '<h2>El número que has pasado es más bajo!</h2>';

    } elseif ( $guessNumber > $numero){

        echo '<h2>El número que has pasado es más alto!</h2>';

    } elseif ( $guessNumber == $numero ){

        echo "<h2>¡Muy bien! el número correcto es $numero!</h2>";
    }

    ?>  


 <!--Formulario AQUÍ para capturar la variable oldGuess -->

<form  method="POST">
    <label for="guess">Introduce un número :</label>
    <input type="text" name="guess" id="guess-form" action="A6Manuel_Martin.php" value="<?= htmlentities($oldGuess) ?>">
    <input type="submit">
</form>


</body>
</html> 
    

